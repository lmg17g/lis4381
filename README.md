> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 - Mobile Web App Development

## Lauren Grassano

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Install AMPPS

    - Provide Screenshots of installations

    - Create Bitbucket Repo

    - Complete Bitbucket tutorial (bitbucketstationlocations)

    - Provide git command descriptions


2. [A2 README.md](a2/README.md "My A2 README.md file")

    -  Create a mobile recipe app using Android Studio

    - Provide screenshots for first user interface

    - Provide screenshots for second user interface

    - Provide screenshots for skillsets

    - Answer Chapter 3 and 4 Questions

3. [A3 README.md](a3/README.md "My A3 README.md file")
    
    - Create a mobile app inside Android Studio that has 2 interfaces and calculates the price of concert tickets
    
    - Provide screenshots for ERD of a petstore, pet, customer
    
    - Provide screenshots for Skill Sets 4-6



4. [A4 README.md](a4/README.md "My A4 README.md file")

    -  Create Bootstrap carousel to go into different assignments

    -  Create a form to collect data and does client-side validation

    -  Link to local lis4381 web app:

    -  Skill Set 10-12


5. [A5 README.md](a5/README.md "My A5 README.md file")
    
    -  Develop server-side validation of a form in php

    -  Develop a form that adds data into a database

    -  Provide screenshots for Skill Sets 13-15


6. [P1 README.md](p1/README.md "My P1 README.md file")

    - Create a mobile app business card
    - Provide Screenshots for both interfaces
    - Provide Screenshots of skill sets

7. [P2 README.md](p2/README.md "My P2 README.md file")
    
    - Add edit and delete functionality to the pet store database that was made in a previous assignment
   
    - Provide screenshots for the pet store, editing a record in the pet store, and creating an RSS feed