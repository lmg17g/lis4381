
# LIS4381 - Mobile Web App Development

## Lauren Grassano

### Assignment 1 Requirements:

## Three parts

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Question (Chs. 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation
* Screenshot of java running Hello World
* Screenshot of Android Studio - My First App
* git commands with short descriptions
* Bitbucket repo link of this assignment

#### Git commands with short descriptions:

1. git status - tells you what is being added or removed from your repository
2. git add - moves changes you have made to a staging area where they will wait for the commit statement     to be implemented
3. git commit - commits all the changes made to the repository, also allows you to name the commit
4. git push - pushes all the commit changes to the git repository
5. git pull - pulls all changes from the remote repository onto the local repository
6. git restore - restores files that have been previously removed if they are still in the tree
7. git clone - clones a remote repository onto the local repository you are working on


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

![AMPPS Installation Screenshot](img/amppsphp.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/lmg17g/bitbucketstationlocations/ "Bitbucket Station Locations")