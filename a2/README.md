
# LIS4381 - Mobile Web App Development

## Lauren Grassano

### Assignment 2 Requirements:

## Four parts

1. Create a mobile recipe app using Android Studio
2. Provide screenshots for first user interface
3. Provide screenshots for second user interface
4. Answer Chapter 3 and 4 Questions

#### README.md file should include the following items:

* Course title, Your name, Assignment Requirements
* Screenshot of running applications first user interface
* Screenshot of running applications second user interface

#### Assignment Screenshots:

| App Main Page |  Recipe Page  |
| ------------- | ------------- |
| ![1st Recipe App](img/image1app.png)  | ![1st Recipe App](img/image2app.png) |


| Skillset 1 |  Skillset2  | Skillset3 |
| ------------- | ------------- | ------------- |
| ![Skillset 1](img/ss1evenorodd.png)  | ![Skillset 2](img/ss2largestnumber.png) | ![Skillset 3](img/ss3arraysandloops.png) |
