
# LIS4381

## Lauren Grassano

### Assignment 4 Requirements:

*Four-Parts:*

1. Create Bootstrap carousel to go into different assignments
2. Create a form to collect data and does client-side validation
3. Link to local lis4381 web app:
4. Skill Set 10-12

#### README.md file should include the following items:

* Screenshot web app
* Screenshot of passed validation
* Screenschot of failed validation


| Main Page |  Passed Validation  |  Failed Validation   |
| ------------- | ------------- | ------------------ |
| ![Main Page](img/main.png)  | ![Passed Validation](img/passed.png) | ![Failed Validation](img/failed.png) |




| Skillset 10 |  Skillset 11  | Skillset 12 |
| ------------- | ------------- | ------------- |
| ![Skillset 4](img/ss10.png)  | ![Skillset 5](img/ss11.png) | ![Skillset 6](img/ss12.png) |



[localhost link](http://localhost/repos/lis4381/ "locahost link")