
# LIS4381

## Lauren Grassano

### Assignment 5 Requirements:

*Four-Parts:*

1. Develop server-side validation of a form in php
2. Develop a form that adds data into a database
3. Chapter Questions (chs. 11, 12, & 19)
4. Skill Set 13-15

#### README.md file should include the following items:

* Screenshot of index.php for the data in petstore
* Screenshot of server-side data validation
* Local lis4381 web app: http://localhost/repos/lis4381/


| Index.php |  Error.php  |
| ------------- | ------------- |
| ![ Index.php ](img/indexphp.png)  | ![error.php](img/errorphp.png) |


| Skill Set 13 |
| ------------- |
| ![Skille Set 13](img/ss13.png)  |


| Screenshot Division |  Division Result  |
| ------------- | ------------- | ------------- |
| ![Screenshot Divison](img/caldivide.png)  | ![Division Result](img/caldivideresult.png) |


| Screenshot Add | Screenshot Add Result |
| ------------- | ------------- |
 ![Screenshot Add](img/caladd.png) | ![Screenshot Add Result](img/caladdresult.png) |




| Read/Write Index |  Read/Write Process  |
| ------------- | ------------- |
| ![Index](img/readwriteindex.png)  | ![Process](img/readwriteprocess.png) |

[localhost link](http://localhost/repos/lis4381/ "locahost link")