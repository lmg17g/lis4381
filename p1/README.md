> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Lauren Grassano

### Project 1 Requirements:

*Six Parts:*

1. Create a launcher icon image and display it in both activities.
2. Add background color to both activities
3. Add text shadow to button
4. Add border image
5. Chapter Questions (7-8)
6. Skill Set 7-9

#### README.md file should include the following items:

* Screenshot of application running first user interface
* Screenshot of application running second user interfacer
* Screenshot of skill sets

#### Assignment Screenshots:

| App Main Page |  App Second Page   |
| ------------- | ------------------ |
| ![Main Page](img/screenshot1.png)  | ![Second page](img/screenshot2.png) |


| Skill Set 7   |  Skill Set 8   |   Skill Set 9 |
| ------------- | ------------------ | ------------------ |
| ![Skillset7](img/ss7.png)  | ![Skillset8](img/ss8.png) | ![Skillset9](img/ss9.png)
