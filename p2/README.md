> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Lauren Grassano

### Project 2 Requirements:

*Six Parts:*

1. Add edit functionality to the pet store database
2. Add deletee functionality to the pet store database
3. Create RSS feed
4. Questions Chapter 13-14


#### README.md file should include the following items:

* Course title, name, assignment requirements
* Screenshot of pet store database on browser
* Screenshot of editing a record in the pet store database
* Screenshot of RSS feed

#### Assignment Screenshots:

|     Index page     |
| ------------------ |
| ![Index page](img/index.png) |


| Edit petstore |  Failed validation | 
| ------------- | ------------------ |
| ![Edit petstore](img/editpetstore.png)  | ![Failed Validation](img/error.png) |

| Passed Validation | Delete Records Prompt |
| ----------------- | --------------------- |
![Passed Validation](img/passedvalidation.png)  | ![Delete Prompt](img/deleteprompt.png) |


| Deleted Record    | RSS feed |
| ----------------- | -------- |
![Deleted Record](img/deleted.png)  | ![RSS Feed](img/rss.png) |
